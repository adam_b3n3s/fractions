# Fractions

## Why?

Have you ever wonder why does computers round decimal numbers, I did.

This is a solution to that made by me. There are probably lot of other libraries that does the same.

## How it works?

1. Inicilize the fractions:

```python
fraction1 = frac(1, 2)
fraction2 = frac(1, 3)
```
2. Add them:

```python
result = fraction1 + fraction2
```

3. Or multiply:

```python
result = fraction1 * fraction2
```
4. ...
