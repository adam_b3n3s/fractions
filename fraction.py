class frac:
    def __init__(self, num, den):
        self.num = num
        self.den = den
        if den == 0:
            raise ZeroDivisionError('Denominator cannot be zero')
        if den < 0:
            self.num *= -1
            self.den *= -1
        if num == 0:
            self.den = 1
        if type(num) != int or type(den) != int:
            raise TypeError('Numerator and denominator must be integers')
        self.simplify()
    def simplify(self):
        g = self.gcd(self.num, self.den)
        self.num //= g
        self.den //= g
    def gcd(self, a, b):
        while b != 0:
            a, b = b, a % b
        return a
    def __str__(self):
        return f"{self.num}/{self.den}"
    def to_float(self):
        return self.num / self.den
    def __add__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        new_num = self.num * other.den + self.den * other.num
        new_den = self.den * other.den
        return frac(new_num, new_den)
    def __sub__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        new_num = self.num * other.den - self.den * other.num
        new_den = self.den * other.den
        return frac(new_num, new_den)
    def __mul__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        new_num = self.num * other.num
        new_den = self.den * other.den
        return frac(new_num, new_den)
    def __truediv__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        if other.num == 0:
            raise ZeroDivisionError('Cannot divide by zero')
        new_num = self.num * other.den
        new_den = self.den * other.num
        return frac(new_num, new_den)
    def __radd__(self, other):
        return frac(other, 1) + self
    def __rsub__(self, other):
        return frac(other, 1) - self
    def __rmul__(self, other):
        return frac(other, 1) * self
    def __rtruediv__(self, other):
        return frac(other, 1) / self
    def __pow__(self, power):
        if power < 0:
            return frac(self.den ** -power, self.num ** -power)
        return frac(self.num ** power, self.den ** power)
    def __lt__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num * other.den < other.num * self.den
    def __eq__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num == other.num and self.den == other.den
    def __gt__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num * other.den > other.num * self.den
    def __le__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num * other.den <= other.num * self.den
    def __ge__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num * other.den >= other.num * self.den
    def __ne__(self, other):
        if (type(other) != frac):
            other = frac(other, 1)
        return self.num != other.num or self.den != other.den
    def __neg__(self):
        return frac(-self.num, self.den)
    def __abs__(self):
        return frac(abs(self.num), abs(self.den))
    def __int__(self):
        return self.num // self.den
    def __float__(self):
        return self.num / self.den
    def __bool__(self):
        return self.num != 0
    def __repr__(self):
        return f"frac({self.num}, {self.den})"
    def __hash__(self):
        return hash((self.num, self.den))
    def __iadd__(self, other):
        return self + other
    def __isub__(self, other):
        return self - other
    def __imul__(self, other):
        return self * other
    def __itruediv__(self, other):
        return self / other
    def __ipow__(self, power):
        return self ** power
    def from_float(f):
        if type(f) != str:
            raise TypeError('Input must be a string')
        try:
            float(f)
        except ValueError:
            raise ValueError('Input must be a valid float')
        else:
            times = 1
            neg = False
            if 'e' in f:
                x = f.split('e')
                f = x[0]
                exp = int(x[1])
                if exp < 0:
                    times = frac(10 ** -exp, 1)
                    neg = True
                else:
                    times = frac(10 ** exp, 1)
            if '.' not in f:
                if neg == True:
                    return frac(int(f), 1)/times
                else:
                    return frac(int(f), 1)*times
            num = f.split('.')[0]
            den = f.split('.')[1]
            if num == '':
                num = '0'
            num += den
            den = '1' + '0' * len(den)
            if neg == True:
                return frac(int(num), int(den))/times
            else:
                return frac(int(num), int(den))*times
        
        