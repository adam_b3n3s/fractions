import unittest
from fraction import frac

class FractionTests(unittest.TestCase):
    def test_addition(self):
        fraction1 = frac(1, 2)
        fraction2 = frac(1, 3)
        result = fraction1 + fraction2
        self.assertEqual(result, frac(5, 6))

    def test_subtraction(self):
        fraction1 = frac(1, 2)
        fraction2 = frac(1, 3)
        result = fraction1 - fraction2
        self.assertEqual(result, frac(1, 6))

    def test_multiplication(self):
        fraction1 = frac(2, 3)
        fraction2 = frac(3, 4)
        result = fraction1 * fraction2
        self.assertEqual(result, frac(1, 2))

    def test_division(self):
        fraction1 = frac(2, 3)
        fraction2 = frac(3, 4)
        result = fraction1 / fraction2
        self.assertEqual(result, frac(8, 9))

    def test_power_positive(self):
        fraction = frac(2, 3)
        result = fraction ** 2
        self.assertEqual(result, frac(4, 9))

    def test_power_negative(self):
        fraction = frac(2, 3)
        result = fraction ** -2
        self.assertEqual(result, frac(9, 4))

    def test_comparison(self):
        fraction1 = frac(1, 2)
        fraction2 = frac(1, 3)
        self.assertTrue(fraction1 > fraction2)
        self.assertFalse(fraction1 < fraction2)
        self.assertTrue(fraction1 >= fraction2)
        self.assertFalse(fraction1 <= fraction2)
        self.assertFalse(fraction1 == fraction2)
        self.assertTrue(fraction1 != fraction2)

    def test_negation(self):
        fraction = frac(2, 3)
        result = -fraction
        self.assertEqual(result, frac(-2, 3))

    def test_absolute(self):
        fraction = frac(-2, 3)
        result = abs(fraction)
        self.assertEqual(result, frac(2, 3))

    def test_integer_conversion(self):
        fraction = frac(5, 2)
        result = int(fraction)
        self.assertEqual(result, 2)

    def test_float_conversion(self):
        fraction = frac(5, 2)
        result = float(fraction)
        self.assertEqual(result, 2.5)

    def test_boolean_conversion(self):
        fraction1 = frac(0, 1)
        fraction2 = frac(3, 4)
        self.assertFalse(bool(fraction1))
        self.assertTrue(bool(fraction2))

    def test_hash(self):
        fraction = frac(3, 4)
        result = hash(fraction)
        self.assertIsInstance(result, int)
    
    def test_float_to_frac_positive(self):
        float_value = "3.14"
        result = frac.from_float(float_value)
        self.assertEqual(result, frac(157, 50))

    def test_float_to_frac_negative(self):
        float_value = "-0.9876"
        result = frac.from_float(float_value)
        self.assertEqual(result, frac(-12345, 12500))

    def test_float_to_frac_scientific_notation_positive(self):
        float_value = "-6.022e3"
        result = frac.from_float(float_value)
        self.assertEqual(result, frac(-6022, 1))

    def test_float_to_frac_scientific_notation_negative(self):
        float_value = "-1234.e-5"
        result = frac.from_float(float_value)
        self.assertEqual(result, frac(-1234, 100000))

if __name__ == '__main__':
    unittest.main()
